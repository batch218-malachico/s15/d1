console.log("Hello World");
// enclose with quotation mark string data
console. log("Hello World");

// case sensitive

console.
log
(
		"Hello Everyone!"
);

// ; delimeter
// we use delimeter to end our line of code

// [comments]
// -single line comments
// shortcut: ctrl + /

// I am a single linr comments

// *Multi-line comment */
/* I am
   a
   multi line
   comment
 */
 // shortcut: ctrl + shift + /*/

 // syntax and statement

 // statements in proramming are instructions that we
 // to computer to perform

 // syntax in programming it is set of rules that describes how statementa muat be considered

 // variables

 /*
it is used to contain data

-syntax in declaring variables
-let/const variables
 */
 let myVariables = "Hello"
 				// assignment operator (=)
 console.log(myVariables);

 let temperature2 = 82;
 console.log(temperature2)
// console.log(hello); will result to not defined error

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign value.
		2. Variable names should start with a lowercase character, use camelcase for multiple words.
		3. For constant variables, us ethe 'const' keyword.
		4. Variables name should be indicative or descriptive of the value being stored.
		5. Nver name a variable starting numbers
		6. Refrain from using space in declaring a variable
*/
// string
let productName = 'desktop computer';
console.log(productName);


let product = "Alvin's computer";
console.log(product);

// number
let productPrice = 18999;
console.log(productPrice);


const interest = 3.539;
console.log(interest)

// reassigning variable value
// syntax
	// variables = newvalue

productName = "laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend);

// interest = 4.89;

const pi = 3.14;
//  pi = 3.16;

// reassigning variables already have a value and we reassigned a new one.
// Initializing - is is our first saving of a value


// declaration
let supplier; 	

// initializing
supplier = "John Smith Trading";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand)

// using a variable with a reserved keyword
// const let = "hello";
// console.log(let);

// section data types


// strings
// strings are series of characters that create a word, phrase, sentence, or anything related to creating text.

// strings in Javascript is enclosed with single (' ') or double (" ") quote

let country ='Philippines';
let province = "Metro Manila"

console.log(province + ', ' + country);
// we use + symbol to concatenate data / values

let fullAddress = province + ', ' + country;
console.log(fullAddress);

// escape Characters (\)
// "\n" refers to creating a new line or set the text to next line

console.log("line1\nline2");

let mailaddress = "Metro Manila\nPhilippines";
console.log(mailaddress);

let message = "John's employee went home early."
console.log(message);

message = 'John\'s employee went home early.'
console.log(message)

// numbers integers or whoe number
let headcount = 26;
console.log(headcount);


// Decimal numbers / float 
let grade = 98.7;
console.log(grade);

// Exponential Notation

let planetDistance = 2e10;
console.log(planetDistance);

console.log("Jhon's grade last quarter is "+ grade);



// arrays
// it is store multiple values with similar data type
let grades = [98.7, 95.4, 90.2, 90.6];
// arrayName //elements
console.log(grades);


// different data types
// storing different data inside an array is not recoomended bacause will not make sense in the context of programming
let details = ["John", "Smith", 32, true];
console.log(details);




// objects
// object are another special kind of data type that is used to mimic real world objects items

// syntax
/*
	let/const objectNmae = {
		propertyA: value,
		propertyB: value
	}

*/

let person = {

	fullname: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: [ "0912 345 6789", "8123 7444"]
	/*
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	*/
};

console.log(person);

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading:90.2,
	fourthGrading:90.6,
};

console.log(myGrades);

let stringValue = "abcd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 630, 700];
// myGrades as object

// 'type of' -
// we use type of operator to retrieve / know the data type

console.log(typeof stringValue);
// output: string

console.log(typeof numberValue);
// output: number

console.log(typeof booleanValue);
// output: boolean

console.log(typeof waterBills);
// output: arrays

console.log(typeof myGrades);
// output: object

// constant objects and arrays
// we cannot reasign the value of the variable but we can change the elements of the constant array
const anime = ["One Piece", "Code Geas", "Monster",
"Demon Slayer", "AOT", "Fairy Tale"];
		// 0 	1 	2 	3 	4 	5 	6 	7
// index - is the position of the element starting zero

anime[0] = "Naruto";
console.log(anime)

// null
// it is used to intentionally express the absence of a value

let spouse = null;
console.log(spouse)

// undefined
// represent the state of a variable that has been declared but without an asigned value

let fullName = undefined
console.log(fullName);



